package com.clubmanage;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AdminCreateClub extends AppCompatActivity {

    private EditText etName, etDescribe;
    private Button btnCreate;
    private SQLiteDatabase database;
    private boolean isEdit;
    private int clubId ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_create_club);
        clubId = getIntent().getIntExtra("id", 0);
        isEdit = getIntent().getBooleanExtra("flag",false);
        etName = findViewById(R.id.et_name);
        etDescribe = findViewById(R.id.et_describe);
        btnCreate = findViewById(R.id.btn_create_club);
        DBUtils dbUtil = new DBUtils(this);
        database = dbUtil.getWritableDatabase();
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getText().toString();
                String describe = etDescribe.getText().toString();
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(describe)) {
                    Toast.makeText(AdminCreateClub.this, "Please input compete information", Toast.LENGTH_SHORT).show();
                } else {
                    if (isEdit){
                        changeClubInfo(name, describe);
                    }else {
                        createClub(name, describe);
                    }
                }
            }
        });
        if (isEdit){
            btnCreate.setText("Confirm");
            getClubDetail();
        }
    }

    private void getClubDetail() {
        Cursor cursor = database.query(DBUtils.TABLE_CLUB, null, "id = ?", new String[]{String.valueOf(clubId)}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                String clubName = cursor.getString(cursor.getColumnIndex("name"));
                String des = cursor.getString(cursor.getColumnIndex("describe"));
                String time = cursor.getString(cursor.getColumnIndex("createdate"));
                etName.setText(clubName);
                etDescribe.setText(des);
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

private void changeClubInfo(String name, String describe){
    ContentValues contentValues = new ContentValues();
    contentValues.put("name", name);
    contentValues.put("describe", describe);
    database.update(DBUtils.TABLE_CLUB, contentValues, "id = ?", new String[]{String.valueOf(clubId)});
    finish();
    Toast.makeText(AdminCreateClub.this, "Modify success", Toast.LENGTH_SHORT).show();
}

private void createClub(String name, String describe) {
    Cursor cursor = database.query(DBUtils.TABLE_CLUB, null, "name = ?", new String[]{name}, null, null, null);
    if (cursor.getCount()!=0){
        Toast.makeText(AdminCreateClub.this, "Club existed", Toast.LENGTH_SHORT).show();
    }else {
        MyApplicaiton myApplicaiton = (MyApplicaiton) getApplication();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("createdate", String.valueOf(System.currentTimeMillis()));
        contentValues.put("createuserid", String.valueOf(MyApplicaiton.userId));
        contentValues.put("createusername", myApplicaiton.getUserInfo().getName());
        contentValues.put("describe", describe);
        database.insert(DBUtils.TABLE_CLUB, null, contentValues);
        database.close();
        Toast.makeText(AdminCreateClub.this, "Created successfully", Toast.LENGTH_SHORT).show();
        finish();
    }
    cursor.close();
}
}
