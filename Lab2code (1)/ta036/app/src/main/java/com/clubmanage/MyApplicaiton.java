package com.clubmanage;

import android.app.Application;

public class MyApplicaiton extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static int userId = 0;
    private UserInfoBean userInfo;

    public UserInfoBean getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoBean userInfo) {
        this.userInfo = userInfo;
    }
}
