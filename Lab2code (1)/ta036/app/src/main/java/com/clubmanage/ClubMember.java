package com.clubmanage;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ClubMember extends AppCompatActivity {

    private int clubid;
    private TextView tvSearch;
    private ListView listView;
    private EditText etSearchName;
    private LinearLayout llEmpty;
    private List<UserInfoBean> list;
    private SQLiteDatabase database;
    private ClubMemberAdapter clubMemberAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.club_member);
        clubid = getIntent().getIntExtra("id", 0);
        tvSearch = findViewById(R.id.tv_search);
        listView = findViewById(R.id.list_view);
        etSearchName = findViewById(R.id.et_search_name);
        llEmpty = findViewById(R.id.ll_empty);
        list = new ArrayList<>();
        clubMemberAdapter = new ClubMemberAdapter(this);
        listView.setAdapter(clubMemberAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ClubMember.this, UserMyInfo.class);
                intent.putExtra("id", list.get(position).getId());
                intent.putExtra("flag", true);
                startActivity(intent);
            }
        });
        DBUtils dbUtils = new DBUtils(this);
        database = dbUtils.getWritableDatabase();
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getClubList(etSearchName.getText().toString());
            }
        });
        getClubList("");
    }

private void getClubList(String keyWord) {
    list.clear();
    Cursor cursor = database.query(DBUtils.TABLE_USER_WITH_CLUB, null, "clubid = ? and username like ?", new String[]{String.valueOf(clubid), "%" + keyWord + "%"}, null, null, null);
    if (cursor.getCount() == 0) {
        listView.setVisibility(View.GONE);
        llEmpty.setVisibility(View.VISIBLE);
    } else {
        listView.setVisibility(View.VISIBLE);
        llEmpty.setVisibility(View.GONE);
        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex("username"));
                int id = cursor.getInt(cursor.getColumnIndex("userid"));
                UserInfoBean userInfoBean = new UserInfoBean();
                userInfoBean.setName(name);
                userInfoBean.setId(id);
                list.add(userInfoBean);
            } while (cursor.moveToNext());
        }
        clubMemberAdapter.setLists(list);
    }
    cursor.close();
}
}
