package com.clubmanage;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnUser,btnAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnUser = findViewById(R.id.btn_user);
        btnAdmin = findViewById(R.id.btn_admin);
        btnUser.setOnClickListener(this);
        btnAdmin.setOnClickListener(this);
        DBUtils dbUtils = new DBUtils(MainActivity.this);
        SQLiteDatabase database = dbUtils.getWritableDatabase();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_user:
                startActivity(new Intent(MainActivity.this,UserLogin.class));
                break;
            case R.id.btn_admin:
                startActivity(new Intent(MainActivity.this,AdminLogin.class));
                break;
        }
    }
}
