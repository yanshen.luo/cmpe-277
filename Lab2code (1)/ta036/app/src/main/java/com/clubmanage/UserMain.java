package com.clubmanage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class UserMain extends AppCompatActivity implements View.OnClickListener {

    private TextView tvMyInfo,tvMyClub,tvClubSearch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_main);
        tvMyInfo = findViewById(R.id.tv_my_info);
        tvMyClub = findViewById(R.id.tv_my_club);
        tvClubSearch = findViewById(R.id.tv_club_search);
        tvMyInfo.setOnClickListener(this);
        tvMyClub.setOnClickListener(this);
        tvClubSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_my_info:
                startActivity(new Intent(UserMain.this,UserMyInfo.class));
                break;
            case R.id.tv_my_club:
                startActivity(new Intent(UserMain.this,UserMyClub.class));

                break;
            case R.id.tv_club_search:
                startActivity(new Intent(UserMain.this,UserClubSearch.class));
                break;
        }
    }
}
