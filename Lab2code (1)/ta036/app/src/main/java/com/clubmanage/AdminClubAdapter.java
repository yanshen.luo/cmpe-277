package com.clubmanage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AdminClubAdapter extends BaseAdapter {

    private List<ClubInfoBean> lists;
    private Context context;
    private int flag; //1隐藏删除按钮

    public AdminClubAdapter(Context context, int flag) {
        this.context = context;
        this.flag = flag;
        lists = new ArrayList<>();
    }

    public void setLists(List<ClubInfoBean> lists) {
        this.lists = lists;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

@Override
public View getView(final int position, View convertView, ViewGroup parent) {
    ViewHolder viewHolder;
    if (convertView == null) {
        viewHolder = new ViewHolder();
        convertView = LayoutInflater.from(context).inflate(R.layout.item_admin_club, null);
        viewHolder.tvName = convertView.findViewById(R.id.tv_club_name);
        viewHolder.ivDelete = convertView.findViewById(R.id.iv_delete);
        convertView.setTag(viewHolder);
    } else {
        viewHolder = (ViewHolder) convertView.getTag();
    }
    if (flag != 1) {
        viewHolder.ivDelete.setVisibility(View.VISIBLE);
        viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(context);
                localBuilder.setTitle("Hint");
                localBuilder.setMessage("Confirm delete club?");
                localBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                        deleteClub(lists.get(position).getId(), position);
                    }
                });
                localBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                    }
                });
                localBuilder.show();
            }
        });
    } else {
        viewHolder.ivDelete.setVisibility(View.GONE);
    }
    viewHolder.tvName.setText(lists.get(position).getName());
    return convertView;
}

private void deleteClub(int id, int pos) {
    DBUtils dbUtils = new DBUtils(context);
    SQLiteDatabase database = dbUtils.getWritableDatabase();
    database.delete(DBUtils.TABLE_CLUB, "id = ?", new String[]{String.valueOf(id)});
    lists.remove(pos);
    notifyDataSetChanged();
}

    class ViewHolder {
        public TextView tvName;
        public ImageView ivDelete;
    }
}
