package com.clubmanage;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UserJoinClub extends AppCompatActivity {

    private int clubId;
    private TextView tvName;
    private TextView tvDescribe;
    private TextView tvTime;
    private TextView tvAdmin;
    private Button btnOption;
    private boolean isJoin;
    private SQLiteDatabase database;
    private int id;
    private String clubName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_join_club);
        clubId = getIntent().getIntExtra("id", 0);
        if (clubId == 0) {
            Toast.makeText(this, "Get club failed", Toast.LENGTH_SHORT).show();
            return;
        }
        tvDescribe = findViewById(R.id.tv_describe);
        tvName = findViewById(R.id.tv_name);
        tvTime = findViewById(R.id.tv_time);
        tvAdmin = findViewById(R.id.tv_admin);
        btnOption = findViewById(R.id.btn_option);
        DBUtils dbUtils = new DBUtils(this);
        database = dbUtils.getWritableDatabase();
        btnOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clubOption();
            }
        });
        checkIsJoin();
        getClubDetail();

    }

    private void clubOption() {
        if (isJoin) {
            android.app.AlertDialog.Builder localBuilder = new AlertDialog.Builder(UserJoinClub.this);
            localBuilder.setTitle("Hint");
            localBuilder.setMessage("Confirm quit club?");
            localBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                    quitClub();
                }
            });
            localBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                }
            });
            localBuilder.show();
        } else {
            MyApplicaiton myApplicaiton = (MyApplicaiton) getApplication();
            ContentValues contentValues = new ContentValues();
            contentValues.put("userid", MyApplicaiton.userId);
            contentValues.put("clubid", clubId);
            contentValues.put("clubname", clubName);
            contentValues.put("username", myApplicaiton.getUserInfo().getName());
            database.insert(DBUtils.TABLE_USER_WITH_CLUB, null, contentValues);
            Toast.makeText(UserJoinClub.this, "Joined successfully", Toast.LENGTH_SHORT).show();
            btnOption.setText("Quit club");
            isJoin = true;
        }
    }

private void quitClub() {
    database.delete(DBUtils.TABLE_USER_WITH_CLUB, "id = ?", new String[]{String.valueOf(id)});
    Toast.makeText(UserJoinClub.this, "Quited successfully", Toast.LENGTH_SHORT).show();
    btnOption.setText("Join club");
    isJoin = false;
}

    private void checkIsJoin() {
        Cursor cursor = database.query(DBUtils.TABLE_USER_WITH_CLUB, null, "userid = ? and clubid = ?"
                , new String[]{String.valueOf(MyApplicaiton.userId), String.valueOf(clubId)}, null, null, null);
        isJoin = cursor.getCount() != 0;
        if (cursor.moveToFirst()) {
            do {
                id = cursor.getInt(cursor.getColumnIndex("id"));
            } while (cursor.moveToNext());
        }
        cursor.close();
        if (isJoin) {
            btnOption.setText("Quit club");
        } else {
            btnOption.setText("Join club");

        }
    }

private void getClubDetail() {
    Cursor cursor = database.query(DBUtils.TABLE_CLUB, null, "id = ?", new String[]{String.valueOf(clubId)}, null, null, null);
    if (cursor.moveToFirst()) {
        do {
            clubName = cursor.getString(cursor.getColumnIndex("name"));
            String des = cursor.getString(cursor.getColumnIndex("describe"));
            String time = cursor.getString(cursor.getColumnIndex("createdate"));
            String admin = cursor.getString(cursor.getColumnIndex("createusername"));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long lt = Long.valueOf(time);
            Date date = new Date(lt);
            tvName.setText(clubName);
            tvDescribe.setText(des);
            tvTime.setText(simpleDateFormat.format(date));
            tvAdmin.setText(admin);
        } while (cursor.moveToNext());
    }
    cursor.close();
}
}
