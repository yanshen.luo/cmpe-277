package com.clubmanage;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBUtils extends SQLiteOpenHelper {

    public static final String DB_NAME = "club.db";//数据库名
    public static final int DB_VERSION = 1;//数据库版本
    public static final String TABLE_USER = "user";//用户表表名
    public static final String TABLE_CLUB = "club";//社团表表名
    public static final String TABLE_ADMIN = "admin";//管理员表表名
    public static final String TABLE_ADMIN_WITH_CLUB = "adminclub";//用户与社团对应表表名
    public static final String TABLE_USER_WITH_CLUB = "userclub";//管理员与社团对应表表名

private static final String SQL_CREATE_USER_TABLE = "create table " +TABLE_USER+"("
        +"id integer primary key autoincrement,"
        +"name text not null,"
        +"pwd text not null,"
        +"realname text not null,"
        +"tel text not null,"
        +"department text not null,"
        +"clazz text not null"
        +");";

private static final String SQL_CREATE_ADMIN_TABLE = "create table " +TABLE_ADMIN+"("
        +"id integer primary key autoincrement,"
        +"name text not null,"
        +"pwd text not null"
        +");";

private static final String SQL_CREATE_CLUB_TABLE = "create table " +TABLE_CLUB+"("
        +"id integer primary key autoincrement,"
        +"name text not null,"
        +"createdate text not null,"
        +"createuserid integer not null,"
        +"createusername text not null,"
        +"describe text not null"
        +");";

private static final String SQL_CREATE_USER_WITH_CLUB_TABLE = "create table " +TABLE_USER_WITH_CLUB+"("
        +"id integer primary key autoincrement,"
        +"userid id not null,"
        +"clubid id not null,"
        +"clubname Text not null,"
        +"username Text not null"
        +");";

    public DBUtils(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

@Override
public void onCreate(SQLiteDatabase db) {
    db.execSQL(SQL_CREATE_USER_TABLE);
    db.execSQL(SQL_CREATE_ADMIN_TABLE);
    db.execSQL(SQL_CREATE_CLUB_TABLE);
    db.execSQL(SQL_CREATE_USER_WITH_CLUB_TABLE);
    ContentValues contentValues = new ContentValues();
    contentValues.put("name","admin");
    contentValues.put("pwd","admin");
    db.insert(DBUtils.TABLE_ADMIN,null,contentValues);
}

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
