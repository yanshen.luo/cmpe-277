package com.clubmanage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ClubMemberAdapter extends BaseAdapter {


    private List<UserInfoBean> lists;
    private Context context;

    public ClubMemberAdapter(Context context) {
        this.context = context;
        lists = new ArrayList<>();
    }

    public void setLists(List<UserInfoBean> lists) {
        this.lists = lists;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

@Override
public View getView(final int position, View convertView, ViewGroup parent) {
    ViewHolder viewHolder;
    if (convertView == null) {
        viewHolder = new ViewHolder();
        convertView = LayoutInflater.from(context).inflate(R.layout.item_club_member, null);
        viewHolder.tvName = convertView.findViewById(R.id.tv_club_name);
        convertView.setTag(viewHolder);
    } else {
        viewHolder = (ViewHolder) convertView.getTag();
    }
    viewHolder.tvName.setText(lists.get(position).getName());
    return convertView;
}

class ViewHolder {
    public TextView tvName;
}
}
