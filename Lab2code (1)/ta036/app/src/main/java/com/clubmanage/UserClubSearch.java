package com.clubmanage;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class UserClubSearch extends AppCompatActivity {

    private TextView tvSearch;
    private ListView listView;
    private EditText etSearchName;
    private LinearLayout llEmpty;
    private List<ClubInfoBean> list;
    private SQLiteDatabase database;
    private AdminClubAdapter adminClubAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_club_search);
        tvSearch = findViewById(R.id.tv_search);
        listView = findViewById(R.id.list_view);
        etSearchName = findViewById(R.id.et_search_name);
        llEmpty = findViewById(R.id.ll_empty);
        list = new ArrayList<>();
        adminClubAdapter = new AdminClubAdapter(this, 1);
        listView.setAdapter(adminClubAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(UserClubSearch.this, UserJoinClub.class);
                intent.putExtra("id", list.get(position).getId());
                startActivity(intent);
            }
        });
        DBUtils dbUtils = new DBUtils(this);
        database = dbUtils.getWritableDatabase();
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getClubList(etSearchName.getText().toString());
            }
        });
        getClubList("");
    }

private void getClubList(String keyWord) {
    list.clear();
    Cursor cursor = database.query(DBUtils.TABLE_CLUB, null, "name like ?", new String[]{"%" + keyWord + "%"}, null, null, null);
    if (cursor.getCount() == 0) {
        listView.setVisibility(View.GONE);
        llEmpty.setVisibility(View.VISIBLE);
    } else {
        listView.setVisibility(View.VISIBLE);
        llEmpty.setVisibility(View.GONE);
        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex("name"));
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                ClubInfoBean clubInfoBean = new ClubInfoBean();
                clubInfoBean.setName(name);
                clubInfoBean.setId(id);
                list.add(clubInfoBean);
            } while (cursor.moveToNext());
        }
        adminClubAdapter.setLists(list);
    }
    cursor.close();
}
}
