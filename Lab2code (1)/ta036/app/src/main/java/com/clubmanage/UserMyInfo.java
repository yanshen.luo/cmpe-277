package com.clubmanage;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class UserMyInfo extends AppCompatActivity {

    private TextView tvRealName, tvTel, tvDepartment, tvClazz;
    private Button btnChange;
    private DBUtils dbUtils;
    private SQLiteDatabase database;
    private boolean isShow;
    private int userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_my_info);
        isShow = getIntent().getBooleanExtra("flag", false);
        if (isShow) {
            userId = getIntent().getIntExtra("id", 0);
        } else {
            userId = MyApplicaiton.userId;
        }
        tvRealName = findViewById(R.id.tv_real_name);
        tvTel = findViewById(R.id.tv_tel);
        tvDepartment = findViewById(R.id.tv_department);
        tvClazz = findViewById(R.id.tv_clazz);
        btnChange = findViewById(R.id.btn_change);
        dbUtils = new DBUtils(this);
        database = dbUtils.getReadableDatabase();
        if (isShow) {
            btnChange.setVisibility(View.GONE);
        } else {
            btnChange.setVisibility(View.VISIBLE);
            btnChange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(UserMyInfo.this, UserMyInfoChange.class));
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserInfo();
    }

private void getUserInfo() {
    Cursor cursor = database.query(DBUtils.TABLE_USER, null, "id = ?", new String[]{String.valueOf(userId)}, null, null, null);
    if (cursor.getCount() == 0) {
        Toast.makeText(UserMyInfo.this, "User does not exist", Toast.LENGTH_SHORT).show();
    } else {
        if (cursor.moveToFirst()) {
            do {
                String realName = cursor.getString(cursor.getColumnIndex("realname"));
                tvRealName.setText(realName);
                String tel = cursor.getString(cursor.getColumnIndex("tel"));
                tvTel.setText(tel);
                String department = cursor.getString(cursor.getColumnIndex("department"));
                tvDepartment.setText(department);
                String clazz = cursor.getString(cursor.getColumnIndex("clazz"));
                tvClazz.setText(clazz);
            } while (cursor.moveToNext());
        }
    }
    cursor.close();
}
}
