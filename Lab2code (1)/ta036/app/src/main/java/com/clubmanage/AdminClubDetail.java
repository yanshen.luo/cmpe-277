package com.clubmanage;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AdminClubDetail extends AppCompatActivity {

    private int clubId;
    private TextView tvName;
    private TextView tvDescribe;
    private TextView tvTime;
    private Button btnOption,btnGetMember;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_club_detail);
        clubId = getIntent().getIntExtra("id", 0);
        if (clubId == 0) {
            Toast.makeText(this, "Get club failed", Toast.LENGTH_SHORT).show();
            return;
        }
        tvDescribe = findViewById(R.id.tv_describe);
        tvName = findViewById(R.id.tv_name);
        tvTime = findViewById(R.id.tv_time);
        btnOption = findViewById(R.id.btn_option);
        btnGetMember = findViewById(R.id.btn_club_member);
        DBUtils dbUtils = new DBUtils(this);
        database = dbUtils.getWritableDatabase();
        btnOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminClubDetail.this, AdminCreateClub.class);
                intent.putExtra("id", clubId);
                intent.putExtra("flag", true);
                startActivity(intent);
            }
        });
        btnGetMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminClubDetail.this, ClubMember.class);
                intent.putExtra("id", clubId);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getClubDetail();
    }

private void getClubDetail() {
    Cursor cursor = database.query(DBUtils.TABLE_CLUB, null, "id = ?", new String[]{String.valueOf(clubId)}, null, null, null);
    if (cursor.moveToFirst()) {
        do {
            String clubName = cursor.getString(cursor.getColumnIndex("name"));
            String des = cursor.getString(cursor.getColumnIndex("describe"));
            String time = cursor.getString(cursor.getColumnIndex("createdate"));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long lt = Long.valueOf(time);
            Date date = new Date(lt);
            tvName.setText(clubName);
            tvDescribe.setText(des);
            tvTime.setText(simpleDateFormat.format(date));
        } while (cursor.moveToNext());
    }
    cursor.close();
}
}
