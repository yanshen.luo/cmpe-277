package com.clubmanage;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UserLogin extends AppCompatActivity implements View.OnClickListener {

    private TextView tvRegister;
    private Button btnLogin;
    private EditText etUserName, etPwd;
    private DBUtils dbUtils;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_login);
        tvRegister = findViewById(R.id.tv_register);
        btnLogin = findViewById(R.id.btn_login);
        etUserName = findViewById(R.id.et_user_name);
        etPwd = findViewById(R.id.et_user_pwd);

        tvRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

        dbUtils = new DBUtils(this);
        database = dbUtils.getWritableDatabase();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_register:
                startActivity(new Intent(UserLogin.this, UserRegister.class));
                finish();
                break;
            case R.id.btn_login:
                if (TextUtils.isEmpty(etUserName.getText().toString()) || TextUtils.isEmpty(etPwd.getText().toString())) {
                    Toast.makeText(UserLogin.this, "Please enter username or password", Toast.LENGTH_SHORT).show();
                } else {
                    login(etUserName.getText().toString(), etPwd.getText().toString());
                }
                break;
        }
    }

    private void login(String userName, String pwd) {
        Cursor cursor = database.query(DBUtils.TABLE_USER, null, "name = ?", new String[]{userName}, null, null, null);
        if (cursor.getCount() == 0) {
            Toast.makeText(UserLogin.this, "User does not exist", Toast.LENGTH_SHORT).show();
        } else {
            String userPwd = "";
            String name = "";
            int id = 0;
            if (cursor.moveToFirst()) {
                do {
                    userPwd = cursor.getString(cursor.getColumnIndex("pwd"));
                    name = cursor.getString(cursor.getColumnIndex("name"));
                    id = cursor.getInt(cursor.getColumnIndex("id"));
                } while (cursor.moveToNext());
            }
            if (TextUtils.isEmpty(userPwd)) {
                Toast.makeText(UserLogin.this, "User does not exist", Toast.LENGTH_SHORT).show();
            } else if (pwd.equals(userPwd)) {
                MyApplicaiton.userId = id;
                UserInfoBean userInfoBean = new UserInfoBean();
                userInfoBean.setName(name);
                MyApplicaiton myApplicaiton = (MyApplicaiton) getApplication();
                myApplicaiton.setUserInfo(userInfoBean);
                Toast.makeText(UserLogin.this, name, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(UserLogin.this, UserMain.class));
                finish();
            } else {
                Toast.makeText(UserLogin.this, "Password incorrect", Toast.LENGTH_SHORT).show();
            }
        }
        cursor.close();
    }
}
