package com.clubmanage;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdminLogin extends AppCompatActivity {

    private EditText etUserName, etPwd;
    private Button btnLogin;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_login);
        etUserName = findViewById(R.id.et_user_name);
        etPwd = findViewById(R.id.et_user_pwd);
        btnLogin = findViewById(R.id.btn_login);
        DBUtils dbUtils = new DBUtils(this);
        database = dbUtils.getReadableDatabase();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etUserName.getText().toString();
                String pwd = etPwd.getText().toString();
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(pwd)) {
                    Toast.makeText(AdminLogin.this, "Please enter username or password", Toast.LENGTH_SHORT).show();
                } else {
                    login(name, pwd);
                }
            }
        });

    }

private void login(String userName, String pwd) {
    Cursor cursor = database.query(DBUtils.TABLE_ADMIN, null, "name = ?", new String[]{userName}, null, null, null);
    if (cursor.getCount() == 0) {
        Toast.makeText(AdminLogin.this, "User does not exist", Toast.LENGTH_SHORT).show();
    } else {
        String userPwd = "";
        int id = 0;
        if (cursor.moveToFirst()) {
            do {
                userPwd = cursor.getString(cursor.getColumnIndex("pwd"));
                id = cursor.getInt(cursor.getColumnIndex("id"));
            } while (cursor.moveToNext());
        }
        if (TextUtils.isEmpty(userPwd)) {
            Toast.makeText(AdminLogin.this, "User does not exist", Toast.LENGTH_SHORT).show();
        } else if (pwd.equals(userPwd)) {
            MyApplicaiton.userId = id;
            UserInfoBean bean = new UserInfoBean();
            bean.setName(userName);
            MyApplicaiton myApplicaiton = (MyApplicaiton) getApplication();
            myApplicaiton.setUserInfo(bean);
            startActivity(new Intent(AdminLogin.this, AdminMain.class));
            finish();
        } else {
            Toast.makeText(AdminLogin.this, "Password incorrect", Toast.LENGTH_SHORT).show();
        }
    }
    cursor.close();
}
}
