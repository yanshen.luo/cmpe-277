package com.clubmanage;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AdminMain extends AppCompatActivity {

    private SQLiteDatabase database;
    private ListView listView;
    private AdminClubAdapter adminClubAdapter;
    private Button btnCreateClub;
    private LinearLayout llEmpty;
    private List<ClubInfoBean> list;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_main);
        DBUtils dbUtils = new DBUtils(this);
        database = dbUtils.getReadableDatabase();
        listView= findViewById(R.id.list_view);
        llEmpty= findViewById(R.id.ll_empty);
        btnCreateClub= findViewById(R.id.btn_create_club);
        list = new ArrayList<>();
        btnCreateClub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminMain.this,AdminCreateClub.class));
            }
        });
        adminClubAdapter = new AdminClubAdapter(this,0);
        listView.setAdapter(adminClubAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(AdminMain.this, AdminClubDetail.class);
                intent.putExtra("id", list.get(position).getId());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getClubList();
    }

    private void getClubList() {
        list.clear();
        Cursor cursor = database.query(DBUtils.TABLE_CLUB, null, "createuserid = ?", new String[]{String.valueOf(MyApplicaiton.userId)}, null, null, null);
        if (cursor.getCount() == 0) {
            listView.setVisibility(View.GONE);
            llEmpty.setVisibility(View.VISIBLE);
        } else {
            listView.setVisibility(View.VISIBLE);
            llEmpty.setVisibility(View.GONE);
            if (cursor.moveToFirst()) {
                do {
                    String name = cursor.getString(cursor.getColumnIndex("name"));
                    int id = cursor.getInt(cursor.getColumnIndex("id"));
                    ClubInfoBean clubInfoBean = new ClubInfoBean();
                    clubInfoBean.setName(name);
                    clubInfoBean.setId(id);
                    list.add(clubInfoBean);
                } while (cursor.moveToNext());
            }
            adminClubAdapter.setLists(list);
        }
        cursor.close();
    }
}
