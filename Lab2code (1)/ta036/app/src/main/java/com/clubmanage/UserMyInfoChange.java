package com.clubmanage;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UserMyInfoChange extends AppCompatActivity {

    private EditText etRealName,etTel,etDepartment,etClazz;
    private Button btnConfirm;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_my_info_change);
        etRealName = findViewById(R.id.et_real_name);
        etTel = findViewById(R.id.et_tel);
        etDepartment = findViewById(R.id.et_department);
        etClazz = findViewById(R.id.et_clazz);
        btnConfirm = findViewById(R.id.btn_confirm);
        DBUtils dbUtils =new DBUtils(this);
        database = dbUtils.getWritableDatabase();
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String realName = etRealName.getText().toString();
                String tel = etTel.getText().toString();
                String department = etDepartment.getText().toString();
                String clazz = etClazz.getText().toString();

                if (TextUtils.isEmpty(realName)||TextUtils.isEmpty(tel)||TextUtils.isEmpty(department)||TextUtils.isEmpty(clazz)){
                    Toast.makeText(UserMyInfoChange.this, "Input cannot be empty", Toast.LENGTH_SHORT).show();
                }else {
                    changeUserInfo(tel,realName,department,clazz);
                }
            }
        });

        getUserInfo();
    }

    private void getUserInfo() {
        Cursor cursor = database.query(DBUtils.TABLE_USER, null, "id = ?", new String[]{String.valueOf(MyApplicaiton.userId)}, null, null, null);
        if (cursor.getCount() == 0) {
            Toast.makeText(UserMyInfoChange.this, "User does not exist", Toast.LENGTH_SHORT).show();
        } else {
            if (cursor.moveToFirst()) {
                do {
                    String realName = cursor.getString(cursor.getColumnIndex("realname"));
                    etRealName.setText(realName);
                    String tel = cursor.getString(cursor.getColumnIndex("tel"));
                    etTel.setText(tel);
                    String department = cursor.getString(cursor.getColumnIndex("department"));
                    etDepartment.setText(department);
                    String clazz = cursor.getString(cursor.getColumnIndex("clazz"));
                    etClazz.setText(clazz);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
    }

    private void changeUserInfo(String tel,String realName,String department,String clazz) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("realname", realName);
        contentValues.put("tel", tel);
        contentValues.put("department", department);
        contentValues.put("clazz", clazz);
        database.update(DBUtils.TABLE_USER, contentValues, "id = ?", new String[]{String.valueOf(MyApplicaiton.userId)});
        finish();
        Toast.makeText(UserMyInfoChange.this, "Successfully modified", Toast.LENGTH_SHORT).show();
    }
}
