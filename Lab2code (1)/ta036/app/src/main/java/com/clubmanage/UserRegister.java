package com.clubmanage;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserRegister extends AppCompatActivity {

    private EditText etUserName, etUserPwd, etUserPwdConfirm, etRealName, etTel, etDepartment, etClazz;
    private Button btnRegister;
    private DBUtils dbUtils;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_register);
        etUserName = findViewById(R.id.et_user_name);
        etUserPwd = findViewById(R.id.et_user_pwd);
        etUserPwdConfirm = findViewById(R.id.et_user_pwd_confirm);
        etRealName = findViewById(R.id.et_real_name);
        etTel = findViewById(R.id.et_tel);
        etDepartment = findViewById(R.id.et_department);
        etClazz = findViewById(R.id.et_clazz);
        btnRegister = findViewById(R.id.btn_register);
        dbUtils = new DBUtils(this);
        database = dbUtils.getWritableDatabase();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = etUserName.getText().toString();
                String pwd = etUserPwd.getText().toString();
                String pwdConfirm = etUserPwdConfirm.getText().toString();
                String realName = etRealName.getText().toString();
                String tel = etTel.getText().toString();
                String department = etDepartment.getText().toString();
                String clazz = etClazz.getText().toString();
                if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(pwd)
                        || TextUtils.isEmpty(pwdConfirm) || TextUtils.isEmpty(realName)
                        || TextUtils.isEmpty(tel) || TextUtils.isEmpty(department)
                        || TextUtils.isEmpty(clazz)) {
                    Toast.makeText(UserRegister.this, "Please enter the registration information", Toast.LENGTH_SHORT).show();
                } else {
                    register(userName, pwd, pwdConfirm, realName, tel, department, clazz);
                }
            }
        });
    }

private void register(String userName, String pwd, String pwdConfirm, String realName, String tel, String department, String clazz) {
    if (!pwd.equals(pwdConfirm)) {
        Toast.makeText(UserRegister.this, "Two passwords entered are not the same", Toast.LENGTH_SHORT).show();
    } else {
        Cursor cursor = database.query(DBUtils.TABLE_USER, new String[]{"name"}, "name = ?", new String[]{userName}, null, null, null);
        if (cursor.getCount() != 0) {
            Toast.makeText(UserRegister.this, "User existed", Toast.LENGTH_SHORT).show();
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put("name", userName);
            contentValues.put("pwd", pwd);
            contentValues.put("realname", realName);
            contentValues.put("tel", tel);
            contentValues.put("department", department);
            contentValues.put("clazz", clazz);
            database.insert(DBUtils.TABLE_USER, null, contentValues);
            getUserId(userName);
        }
        cursor.close();
    }
}

    private void getUserId(String userName) {
        Cursor cursor = database.query(DBUtils.TABLE_USER, null, "name = ?", new String[]{userName}, null, null, null);
        if (cursor.getCount() == 0) {
            Toast.makeText(UserRegister.this, "User does not exist", Toast.LENGTH_SHORT).show();
        } else {
            if (cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex("id"));
                    MyApplicaiton.userId = id;
                    UserInfoBean userInfoBean = new UserInfoBean();
                    userInfoBean.setName(userName);
                    MyApplicaiton myApplicaiton = (MyApplicaiton) getApplication();
                    myApplicaiton.setUserInfo(userInfoBean);
                    startActivity(new Intent(UserRegister.this, UserMain.class));
                    finish();
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
    }
}
